const gulp = require('gulp');
const rimraf = require('rimraf');

gulp.task('delete', function(done) {
  rimraf('./dist', done);
});