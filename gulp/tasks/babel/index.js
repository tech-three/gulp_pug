const gulp = require('gulp');
const uglify = require('gulp-uglify');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const babelify = require('babelify');

const path = {
  dstDir: './dist/js',
  srcDir: './src/js/index.js'
}

gulp.task('babel', function() {
  return browserify(path.srcDir)
    .transform(babelify.configure({
      presets: ["@babel/env"],
      sourceType: "module"
    }))
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest(path.dstDir));
});

gulp.task('babel:compress', function() {
  return gulp.src('./dist/js/bundle.js')
    .pipe(uglify())
    .pipe(gulp.dest(path.dstDir));
});