const gulp = require('gulp');
const requireDir = require('require-dir');

requireDir('./gulp/tasks', {recurse: true});

// ローカルサーバー起動
gulp.task('server', gulp.series('browser-sync'));

//　本番モードでビルド
/**
 * 1.distフォルダ削除
 * 2.pug→html変換して圧縮
 * 3.scss→css変換して圧縮
 * 4.ES6→es5に変換
 * 5.js圧縮
 * 6.image圧縮
 */
gulp.task('build', gulp.series('delete', 'build:pug', 'build:scss', 'babel', 'babel:compress', 'imagemin'));